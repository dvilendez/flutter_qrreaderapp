import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import 'package:qrreaderapp/src/models/scan_model.dart';
export 'package:qrreaderapp/src/models/scan_model.dart';


class DBProvider {

  static Database _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDB();

    return _database;
  }

  initDB() async {

    Directory documentsDirectory = await getApplicationDocumentsDirectory();

    final path = join(documentsDirectory.path, 'scansDB.db');

    return await openDatabase(
      path,
      version: 2,
      onOpen: (db) {},
      onCreate: (Database db, int version) async {
        await db.execute(
          'CREATE TABLE Scans ('
          ' id INTEGER PRIMARY KEY AUTOINCREMENT,'
          ' type TEXT,'
          ' value TEXT'
          ')'
        );
      }
    );
  }

  // INSERT
  
  newScanRaw (ScanModel newScan) async {
    final db = await database;
    final res = await db.rawInsert(
      "INSERT Into Scans (type, value) "
      "VALUES ('${newScan.type}', '${newScan.value}')"
    );
    return res;
  }

  newScan (ScanModel newScan) async {
    final db = await database;
    final res = await db.insert('Scans', newScan.toJson());
    return res;
  }

  // SELECT

  Future<ScanModel> getScanById (int id) async {
    final db = await database;
    final res = await db.query('Scans', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty ? ScanModel.fromJson(res.first) : null;
  }

  Future<List<ScanModel>> getAllScans () async {
    final db = await database;
    final res = await db.query('Scans');

    List<ScanModel> scanList = res.isNotEmpty ? res.map((item) => ScanModel.fromJson(item)).toList() : [];

    return scanList;
  }

  Future<List<ScanModel>> getAllScansByType (String type) async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Scans WHERE type ='$type'");

    List<ScanModel> scanList = res.isNotEmpty ? res.map((item) => ScanModel.fromJson(item)).toList() : [];

    return scanList;
  }

  //UPDATE

  Future<int> updateScan (ScanModel newScan) async {
    final db = await database;
    final res = await db.update('Scans', newScan.toJson(), where: 'id = ?', whereArgs: [newScan.id]);
    return res;
  }

  //DELETE

  Future<int> deleteScan (int id) async {
    final db = await database;
    final res = await db.delete('Scans', where: 'id = ?', whereArgs: [id]);
    return res;
  }

  Future<int> deleteAllScans () async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Scans');
    return res;
  }

}