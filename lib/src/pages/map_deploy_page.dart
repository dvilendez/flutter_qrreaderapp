import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';

import 'package:qrreaderapp/src/models/scan_model.dart';

class MapDeployPage extends StatefulWidget {

  @override
  _MapDeployPageState createState() => _MapDeployPageState();
}

class _MapDeployPageState extends State<MapDeployPage> {
  final map = new MapController();

  String mapType = 'streets';

  @override
  Widget build(BuildContext context) {

    final ScanModel scan = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text('Coords QR'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.my_location),
            onPressed: () {
              map.move(scan.getLatLng(), 15);
            },
          )
        ],
      ),
      body: _createFlutterMap(scan),
      floatingActionButton: _createFloatingButton(context),
    );
  }

  Widget _createFloatingButton(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.repeat),
      backgroundColor: Theme.of(context).primaryColor,
      onPressed: () {
        print(mapType);
        
        setState(() {
          if (mapType == 'streets') {
            mapType = 'dark';
          } else if(mapType == 'dark') {
            mapType = 'light';
          } else if(mapType == 'light') {
            mapType = 'outdoors';
          } else if(mapType == 'outdoors') {
            mapType = 'satellite';
          } else if(mapType == 'satellite') {
            mapType = 'streets';
          }
        });
        print(mapType);
      },
    );
  }

  Widget _createFlutterMap(ScanModel scan) {
    return FlutterMap(
      mapController: map,
      options: MapOptions(
        center: scan.getLatLng(),
        zoom: 15
      ),
      layers: [
        _createMap(),
        _createMarkers(scan)
      ],
    );
  }

  _createMap() {
    return TileLayerOptions(
      urlTemplate: 'https://api.mapbox.com/v4/'
      '{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}',
      additionalOptions: {
        'accessToken': 'pk.eyJ1IjoiZHZpbGVuZGV6IiwiYSI6ImNrOHhuNHB6azBwNXQzZW9lejkxZnhrYnEifQ.LQvTuZZAymMRsF54usJM4w',
        'id': 'mapbox.$mapType'
        // streets, dark, light, outdoors, satellite
      }
    );
  }

  _createMarkers(ScanModel scan) {
    return MarkerLayerOptions(
      markers: <Marker>[
        Marker(
          width: 100.0,
          height: 100.0,
          point: scan.getLatLng(),
          builder: (context) => Container(
            child: Icon(
              Icons.location_on,
              size: 60.0,
              color: Theme.of(context).primaryColor
            ),
          )
        )
      ]
    );
  }
}